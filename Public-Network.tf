
/*
  Public Subnet 
*/
resource "aws_subnet" "public" {
  vpc_id                  = "${aws_vpc.default.id}"
  availability_zone       = "${availability_zone}"
  cidr_block              = "${var.vpc_cidr}"
  map_public_ip_on_launch = false
  tags {
    Name = "public"
  }
}
resource "aws_route_table" "public" {
  vpc_id = "${aws_vpc.default.id}"


route {
        cidr_block = "0.0.0.0/0"
        gateway_id = "${aws_internet_gateway.default.id}"
    }


  tags {
    Name = "public"
  }
}



resource "aws_route_table_association" "public" {
vpc_id = "${aws_vpc.default.id}"
  route_table_id = "${aws_route_table.public.id}"
}
