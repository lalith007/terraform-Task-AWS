resource "aws_instance" "elastic" {
    ami = "${var.ami}"

   instance_type = "${var.elasticsearch_instance_type}"



   security_groups = ["${aws_security_group.allow_all.id}","${aws_security_group.default.id}"]


    tags {
        Name = "Elasticsearch"
    }


}

resource "aws_instance" "logstash" {
    ami = "${var.ami}"


    instance_type = "${var.logstash_instance_type}"

  depends_on = ["aws_instance.elastic"]


security_groups = ["${aws_security_group.allow_all.id}","${aws_security_group.default.id}"]



    tags {
        Name = "Logstash"
    }


}


resource "aws_instance" "kibana" {
    ami = "${var.ami}"
    instance_type = "${var.kibana_instance_type}"


  depends_on = ["aws_instance.elastic"]


security_groups = ["${aws_security_group.allow_all.id}","${aws_security_group.default.id}","${aws_security_group.allow_from_specific.id}"]


    tags {
        Name = "Kibana"
    }


}

