variable "aws_access_key" {}
variable "aws_secret_key" {}

variable "aws_key_path" {}
variable "aws_key_name" {}
variable "aws_region" {
    description = "EC2 Region for the VPC"
    default = "us-east-1"
}

variable "ami" {
    description = "AMIs of Ubuntu"
        default = "ami-c70f90d0"

}





variable "vpc_cidr" {
    description = "CIDR for the whole VPC"
    default = "10.0.0.0/16"
}


variable "availability_zone" {
  description = "AWS availability zone"
  default = "us-east-1a"
}




variable "private_subnet" {
  description = "AWS Private Subnet"
  default = "10.0.1.100/30"
}




variable "office_network" {
  description = "Allow to access kibana from office network"
  default = "202.71.0.6/32"
}



variable "elasticsearch_instance_type" {
  description = "Instance Type of Elasticsearch"
  default = "t2.micro"
}


variable "logstash_instance_type" {
  description = "Instance Type of Logstash"
  default = "t2.micro"
}


variable "kibana_instance_type" {
  description = "Instance Type of Kibana"
  default = "t2.micro"
}


