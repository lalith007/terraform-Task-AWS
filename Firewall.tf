resource "aws_security_group" "allow_all" {
  name = "allow_all"
  description = "Allow all outgoing traffic"



  egress {
      from_port = 80
      to_port = 80
      protocol = "-1"
      cidr_blocks = ["0.0.0.0/0"]
  }


  egress {
      from_port = 443
      to_port = 443
      protocol = "-1"
      cidr_blocks = ["0.0.0.0/0"]
  }






}


resource "aws_security_group" "allow_from_specific" {
  name = "allow_specific"
  description = "Allow Incoming traffic from specfic network"



  ingress {
      from_port = 5601
      to_port = 5601
      protocol = "tcp"
      cidr_blocks = ["${var.office_network}"]
  }





}



resource "aws_security_group" "default" {
  name = "default-internal"
  description = "Default security group that allows inbound and outbound traffic from all instances in the VPC"
  vpc_id = "${aws_vpc.default.id}"

  ingress {
    from_port   = "0"
    to_port     = "0"
    protocol    = "-1"
    self        = true
  }

  egress {
    from_port   = "0"
    to_port     = "0"
    protocol    = "-1"
    self        = true
  }

  tags {
    Name = "internal-default-vpc"
  }
}

